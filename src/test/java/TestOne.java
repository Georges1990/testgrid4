import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import PO.BasePage;
/*****************************************************************************
 * Author:      Onur Baskirt
 * Description: This is the first Selenium TestNG test.
 *              It opens swtestacademy homepage and prints and checks its title.
 *******************************************************************************/
public class TestOne {
    //-----------------------------------Global Variables-----------------------------------
    //Declare a Webdriver variable

    public WebDriver driver;
    public String testURL = "http://www.swtestacademy.com/";
    BasePage BP;

    //-----------------------------------Test Setup-----------------------------------
    @BeforeMethod
    public void setupTest () throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");

        /* The execution happens on the Selenium Grid with the address mentioned earlier */
        driver = new RemoteWebDriver(new URL("http://172.21.0.1:4444"), caps);
        BP = new BasePage(driver);
        driver.navigate().to(testURL);
    }
    //-----------------------------------Tests-----------------------------------
    @Test
    public void firstTest () throws Exception {
        //Get page title
        String title = driver.getTitle();
        //Print page's title
        System.out.println("Page Title: " + title);
        //Assertion
        Assert.assertEquals(title, "Software Test Academy", "Title assertion is failed!");
        BP.CapturaImagen(driver, "fotoPrueba_1");
    }
    @Test
    public void secondTest () throws Exception {
        //Get page title
        String title = driver.getTitle();
        //Print page's title
        System.out.println("Page Title: " + title);
        //Assertion
        Assert.assertEquals(title, "Software Test Academy", "Title assertion is failed!");
        BP.CapturaImagen(driver, "fotoPrueba_2");
    }
    //-----------------------------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest (){
        //Close browser and end the session
        driver.quit();
    }
}